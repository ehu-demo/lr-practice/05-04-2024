﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BankSystemLogic.v._3;

/// <summary>
/// Represents a bank account owner.
/// </summary>
public class AccountOwner
{
    private readonly string email;
    private readonly string firstName;
    private readonly string lastName;
    private readonly List<BankAccount> accounts = new();

    /// <summary>
    /// Initializes a new instance of the <see cref="AccountOwner"/> class with the specified account owner <param name="email"/>, <param name="firstName"/> and <param name="lastName"/>.
    /// </summary>
    /// <param name="email">Account owner email.</param>
    /// <param name="firstName">Account owner first name.</param>
    /// <param name="lastName">Account owner last name.</param>
    /// <exception cref="ArgumentException"><param name="email"/>is null, empty, consists only of white-space characters or is not in a recognized format.</exception>
    /// <exception cref="ArgumentException"><param name="firstName"/> is null, empty or consists only of white-space characters.</exception>
    /// <exception cref="ArgumentException"><param name="lastName"/> is null, empty or consists only of white-space characters.</exception>
    public AccountOwner(string? email, string? firstName, string? lastName) => (Email, FirstName, LastName) = (email, firstName, lastName);

    /// <summary>
    /// Account owner e-mail.
    /// </summary>
    /// <exception cref="ArgumentException"><paramref name="value"/> is null, empty, consists only of white-space characters or is not in a recognized format.</exception>
    public string Email
    {
        get => this.email;
        init
        {
            VerifyEmail(value, nameof(value));
            email = value;
        }
    }

    /// <summary>
    /// Account owner first name.
    /// </summary>
    /// <exception cref="ArgumentException"><paramref name="value"/> is null, empty or consists only of white-space characters.</exception>
    public string FirstName
    {
        get => firstName;
        init
        {
            VerifyString(value, nameof(value));
            firstName = value;
        }
    }

    /// <summary>
    /// Account owner last name.
    /// </summary>
    /// <exception cref="ArgumentException"><paramref name="value"/> is null, empty or consists only of white-space characters.</exception>
    public string LastName
    {
        get => lastName;
        init
        {
            VerifyString(value, nameof(value));
            lastName = value;
        }
    }

    /// <summary>
    /// Return information about accounts of the owner.
    /// </summary>
    public BankAccount[] Accounts => accounts.ToArray();

    /// <summary>
    /// Add new account to the owner.
    /// </summary>
    /// <param name="account"></param>
    public void Add(BankAccount account) => accounts.Add(account);

    /// <summary>
    /// Get report about account owner.
    /// </summary>
    public override string ToString() => $"{this.FirstName} {this.LastName}, {this.Email}.";

    private static void VerifyString(string value, string paramName)
        => _ = string.IsNullOrWhiteSpace(value)
            ? throw new ArgumentException($"{paramName} is null or white space.", paramName)
            : value;

    private static void VerifyEmail(string email, string paramName)
    {
        try
        {
            _ = new MailAddress(email);
        }
        catch (Exception)
        {
            throw new ArgumentException($"{paramName} is invalid.", paramName);
        }
    }
}