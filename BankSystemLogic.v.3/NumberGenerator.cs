﻿namespace BankSystemLogic.v._3;

/// <summary>
/// Represents an account number generator.
/// </summary>
public class NumberGenerator
{
    private static readonly NumberGenerator instance;
    private int lastNumber = 1234567890;

    static NumberGenerator() => instance = new NumberGenerator();

    private NumberGenerator()
    {
    }

    /// <summary>
    /// Get  
    /// </summary>
    public static NumberGenerator Instance => instance;

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public string Generate() => (++lastNumber).ToString();
}