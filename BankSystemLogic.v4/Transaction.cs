﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystemLogic.v._4;

/// <summary>
/// Represents a bank account transaction.
/// </summary>
public class Transaction
{
    /// <summary>
    /// Amount of the transaction.
    /// </summary>
    public decimal Amount { get; }

    /// <summary>
    /// Data of the transaction.
    /// </summary>
    public DateTime Date { get; }

    /// <summary>
    /// Information of the transaction.
    /// </summary>
    public string Note { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="Transaction"/> class with the specified <param name="amount"/>, <param name="date"/> and <param name="note"/>.
    /// </summary>
    /// <param name="amount">Amount of the transaction.</param>
    /// <param name="date">Date of the transaction.</param>
    /// <param name="note">Note of the transaction.</param>
    public Transaction(decimal amount, DateTime date, string note) => (Amount, Date, Note) = (amount, date, note);

    public override string ToString() =>
        $"{Date} {Note} : {(Amount < 0 ? $"Debited from account {this.Amount}." : $"Credited to account {this.Amount}.")}";
}