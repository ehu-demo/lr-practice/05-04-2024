﻿using System.Globalization;

namespace BankSystemLogic;

public class BankAccount
{
    private readonly string number;
    private readonly string currency;
    private readonly string owner;//?
    private decimal balance;
    private static int accountNumberSeed = 123456789;

    public BankAccount(string? owner, string? currency) => (this.Owner, this.Currency, this.number) = (owner, currency, accountNumberSeed++.ToString());
    //{
    //    this.Owner = owner;
    //    this.Currency = currency;
    //    this.number = accountNumberSeed++.ToString();
    //}

    public BankAccount(string? owner, string? currency, decimal amount) 
        : this(owner, currency)// => this.Deposit(amount);
    {
        this.Deposit(amount);
    }

    public string Number => this.number;

    public decimal Balance => this.balance;

    public string Owner
    {
        get => this.owner;
        init => this.owner = !string.IsNullOrWhiteSpace(value) ? value : throw new ArgumentException($"{value} cannot be empty.", nameof(value));
    }

    public string Currency
    {
        get => this.currency;//?
        init => this.currency = !string.IsNullOrWhiteSpace(value) && CurrencyIsValid(value) ? value : throw new ArgumentException($"Invalid currency notation.", nameof(value));
    }

    public void Deposit(decimal amount)
    {
        if (amount <= 0)
        {
            throw new ArgumentOutOfRangeException(nameof(amount));
        }

        this.balance += amount;
    }

    public void Withdraw(decimal amount)
    {
        if (amount <= 0)
        {
            throw new ArgumentOutOfRangeException(nameof(amount));
        }

        if (this.Balance - amount < 0)
        {
            throw new InvalidOperationException("");
        }

        this.balance -= amount;
    }

    public string GetInformation() => $"No {this.Number} for {this.Owner} with {this.Balance} {this.Currency}";

    private bool CurrencyIsValid(string currency)
    {
        CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

        foreach (CultureInfo culture in cultures)
        {
            RegionInfo regionInfo = new(culture.Name);

            if (regionInfo.ISOCurrencySymbol == currency)
            {
                return true;
            }
        }

        return false;
    }
}