﻿using BankSystemLogic.v._2;

var owner = new AccountOwner("anzhelika.kravchuk@gmail.com", "Anzhelika", "Kravchuk");
var bankAccount1 = new BankAccount(owner, "USD");

bankAccount1.Deposit(123m, DateTime.Now, "Account Top-Up");
bankAccount1.Withdraw(23, DateTime.Now, "Google Pay");
Console.WriteLine(bankAccount1.ToString());

owner = new AccountOwner("anzhelika.kravchuk@gmail.com", "Anzhelika", "Kravchuk");
var bankAccount2 = new BankAccount(owner, "BYN");
bankAccount2.Deposit(1200m, DateTime.Now, "Account Top-Up");
bankAccount2.Withdraw(203, DateTime.Now, "Google Pay");
Console.WriteLine(bankAccount2.ToString());

bankAccount1.Withdraw(67, DateTime.Now, "Google Pay");
Console.WriteLine(bankAccount1.ToString());

var owner1 = new AccountOwner("anzhelika@gmail.com", "Anzhelika", "Kravchuk");
var bankAccount3 = new BankAccount(owner1, "BYN");
bankAccount3.Deposit(67, DateTime.Now, "Google Pay");
Console.WriteLine(bankAccount3.ToString());

owner1 = new AccountOwner("anzhelika@gmail.com", "Anzhelika", "Kravchuk");
var bankAccount4 = new BankAccount(owner1, "BYN");
bankAccount4.Deposit(123, DateTime.Now, "Google Pay");
Console.WriteLine(bankAccount4.ToString());