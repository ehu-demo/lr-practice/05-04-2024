﻿using System.Globalization;

namespace BankSystemLogic.v._2;

public class BankAccount
{
    private readonly string number;
    //private decimal balance;
    private readonly string currency;
    private AccountOwner accountOwner;
    private readonly List<Transaction> transactions;

    /// <summary>
    /// Initializes a new instance of the <see cref="BankAccount"/> class with the specified account number and account owner.
    /// </summary>
    /// <param name="accountOwner">account owner.</param>
    /// <exception cref="ArgumentException"><paramref name="accountOwner"/> is null, empty or consists only of white-space characters.</exception>
    public BankAccount(AccountOwner? accountOwner, string? currency)
    {
        this.AccountOwner = accountOwner;
        this.Currency = currency;
        this.number = NumberGenerator.Instance.Generate();
        this.transactions = new List<Transaction>();
    }

    /// <summary>
    /// Get bank account number.
    /// </summary>
    public string Number => number;

    /// <summary>
    /// Get bank account balance.
    /// </summary>
    public decimal Balance
    {
        get
        {
            var result = 0m;
            foreach (var transaction in transactions)
            {
                result += transaction.Amount;
            }

            return result;
        }
    }

    /// <summary>
    /// Get or init bank account currency.
    /// </summary>
    /// <exception cref="ArgumentException"></exception>
    public string Currency
    {
        get => this.currency;
        init => this.currency = !string.IsNullOrWhiteSpace(value) && IsCurrencyValid(value)
            ? value
            : throw new ArgumentException("", nameof(value));
    }

    /// <summary>
    /// Get or init bank account owner.
    /// </summary>
    /// <exception cref="ArgumentNullException"><paramref name="value"/> is null.</exception>
    public AccountOwner? AccountOwner
    {
        get => accountOwner;
        init
        {
            accountOwner = value ?? throw new ArgumentNullException(nameof(value));
            accountOwner.Add(this);
        }
    }

    /// <summary>
    /// Returns a account string representation.
    /// </summary>
    /// <returns>String representation of a account.</returns>
    public override string ToString() => $"{this.AccountOwner} No:{this.Number}. Balance: {this.Balance}{this.Currency}.\n";

    /// <summary>
    /// Credits money to the account.
    /// </summary>
    /// <param name="amount">deposit money.</param>
    /// <param name="date"></param>
    /// <param name="note"></param>
    /// <exception cref="ArgumentException"><paramref name="amount"/> balance &lt;= 0.</exception>
    public void Deposit(decimal amount, DateTime date, string note)
    {
        if (amount <= 0)
        {
            throw new ArgumentOutOfRangeException(nameof(amount), "Amount of deposit must be positive");
        }

        var transaction = new Transaction(amount, date, note);
        transactions.Add(transaction);
        SendInformation(transaction);
        //this.balance += amount;
    }

    /// <summary>
    /// Withdraws money from the account.
    /// </summary>
    /// <param name="amount">withdrawal amount.</param>
    /// <param name="date">withdrawal date.</param>
    /// <param name="note">withdrawal note.</param>
    /// <exception cref="ArgumentException"><paramref name="amount"/> balance &lt;= 0.</exception>
    public void Withdraw(decimal amount, DateTime date, string note)
    {
        if (amount <= 0)
        {
            throw new ArgumentOutOfRangeException(nameof(amount), "Amount of withdrawal must be positive");
        }

        if (this.Balance - amount < 0)
        {
            throw new InvalidOperationException("Amount of withdrawal must be positive");
        }

        var transaction = new Transaction(-amount, date, note);
        transactions.Add(transaction);
        SendInformation(transaction);
        //balance -= amount;
    }

    public void SendInformation(Transaction transaction) => Console.WriteLine(transaction);

    private static bool IsCurrencyValid(string currency)
    {
        CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

        foreach (CultureInfo culture in cultures)
        {
            RegionInfo regionInfo = new(culture.Name);

            if (regionInfo.ISOCurrencySymbol == currency)
            {
                return true;
            }
        }

        return false;
    }
}
